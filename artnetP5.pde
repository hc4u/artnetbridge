import themidibus.*;
import oscP5.*;
import netP5.*;

OscP5 oscP5;
NetAddress oscATEM;
MidiBus mbProPresenter;

OscMessage me1Key1on;
OscMessage me1Key1off;
OscMessage me1Key2on;
OscMessage me1Key2off;

final static int anATEM = 510; // DMX Channel for ATEM Control
final static int anProPresenter = 511; // DMX Channel for ProPresenter
final static int chProPresenter = 1; // MIDI Channel for ProPresenter
int lvProPresenter = -1;

int iATEM1Program = -1;
int iATEM1Preview = -1;
int iATEM2Program = -1;
int iATEM2Preview = -1;
int lviATEM1Program = 0;
int iAUX1 = -1;
int iAUX2 = -1;
int iAUX3 = -1;
int iAUX4 = -1;
int iAUX5 = -1;
int iAUX6 = -1;

boolean bATEM1USK1Status = false;
boolean bATEM1USK2Status = false;
boolean bATEM2USK1Status = false;
boolean bATEM2USK2Status = false;
boolean bATEM1DSK1Status = false;
boolean bATEM1DSK2Status = false;

ArtNetListener artNetListener;
byte[] inputDmxArray;

// Previous values for ATEM & PP5 channels
int lvATEM = -1;

void setKeyer(int k, int v) {
  if(k == 2) {
    if(v > 160) {
      oscP5.send(me1Key2on, oscATEM);
      bATEM1USK2Status = true;
    } else if(v < 80) {
      oscP5.send(me1Key2off, oscATEM);
      bATEM1USK2Status = false;
    }
  }
}

void setup() {
  size( 480, 160);
  textSize( 16);

  println( "Starting ...");

  /* start oscP5, listening for incoming messages at port 4444, this is for oscATEM to communicate with */
  oscP5 = new OscP5(this,"192.168.10.19", 4444, OscProperties.UDP);
  
  /* oscATEM is a NetAddress. a NetAddress takes 2 parameters,
   * an ip address and a port number. myRemoteLocation is used as parameter in
   * oscP5.send() when sending osc packets to another computer, device, 
   * application. usage see below. for testing purposes the listening port
   * and the port of the remote location address are the same, hence you will
   * send messages back to this sketch.
   */
  oscATEM = new NetAddress("192.168.10.19",3333);
  me1Key1on = new OscMessage("/atem/setusk/1");
  me1Key1on.add(1.0);
  me1Key1off = new OscMessage("/atem/setusk/1");
  me1Key1off.add(0.0);
  me1Key2on = new OscMessage("/atem/setusk/2");
  me1Key2on.add(1.0);
  me1Key2off = new OscMessage("/atem/setusk/2");
  me1Key2off.add(0.0);
 
  setKeyer(2, 0); // Lyrics / Graphics

  // Artnet Listener
  artNetListener = new ArtNetListener();
  
  mbProPresenter = new MidiBus(this, "ProPresenter");
  mbProPresenter.addOutput("ProPresenter");
}

void exit() {
  println( "Exiting ...");
  artNetListener.stopArtNet();
  super.exit();
}

String ATEMInputName(int inputID) {
  switch(inputID) {
    case 0: return "Black";
    case 1: return "Kidstown Numbers";
    case 2: return "Camera 1";
    case 3: return "Camera 2";
    case 4: return "Camera 3";
    case 5: return "Camera 4";
    case 6: return "Camera 5";
    case 7: return "Camera 6";
    case 8: return "AUX Hyperdeck";
    case 9: return "Front of House";
    case 10: return "Sermon Notes Fill";
    case 11: return "SDI 11";
    case 12: return "SDI 12";
    case 13: return "SDI 13";
    case 14: return "SDI 14";
    case 15: return "Resolume Arena 1";
    case 16: return "Resolume Arena 2";
    case 17: return "SDI 17";
    case 18: return "Hyperdeck 1";
    case 19: return "Hyperdeck 2";
    case 20: return "Hyperdeck 3";
    case 1000: return "Color Bars";
    case 2001: return "Color Gen 1";
    case 2002: return "Color Gen 2";
    case 3010: return "MP1 Fill";
    case 3011: return "MP1 Alpha";
    case 3020: return "MP2 Fill";
    case 3021: return "MP2 Alpha";
    case 6000: return "Supersource";
    case 7001: return "Clean Feed 1";
    case 7002: return "Clean Feed 2";
    case 8001: return "AUX 1";
    case 8002: return "AUX 2";
    case 8003: return "AUX 3";
    case 8004: return "AUX 4";
    case 8005: return "AUX 5";
    case 8006: return "AUX 6";
    case 10010: return "ME1 Program";
    case 10011: return "ME1 Preview";
    case 10020: return "ME2 Program";
    case 10021: return "ME2 Preview";
  };
 return "Unknown Input " + inputID;
 }

void draw() {
  int nvATEM;
  int nvProPresenter;
  
  inputDmxArray = artNetListener.getCurrentInputDmxArray(); 
  nvATEM = artNetListener.toInt(inputDmxArray[anATEM]);
  nvProPresenter = artNetListener.toInt(inputDmxArray[anProPresenter]);
  int ppPlaylist = (nvProPresenter >> 4) + 1;
  int ppSlide = (nvProPresenter & 0xf) + 1;

  background( 0);
  fill( 255);
  text( "Pro5 DMX: " + nvProPresenter, 20, 20);
  text( "ATEM DMX: " + nvATEM, 20, 90);

  if(nvProPresenter != lvProPresenter) {
    lvProPresenter = nvProPresenter;
    if(nvProPresenter < 241) {
      mbProPresenter.sendNoteOn(chProPresenter, 19, ppPlaylist);
      mbProPresenter.sendNoteOn(chProPresenter, 20, ppSlide);
      mbProPresenter.sendNoteOff(chProPresenter, 19, ppPlaylist);
      mbProPresenter.sendNoteOff(chProPresenter, 20, ppSlide);
    }    
  }
  text( "> Playlist: ", 40, 40);
  text( ppPlaylist, 140, 40);
  text( "> Slide: ", 40, 60);
  text( ppSlide, 140, 60);

  if(nvATEM>160) {
    text( "> Lyrics On", 40, 110);
  } else if(nvATEM<80) {
    text( "> Lyrics Off", 40, 110);
  } else {
    text( "> Lyrics Unchanged", 40, 110);
  }

  text( "> Program: ", 40, 130);
  text( ATEMInputName(iATEM1Program), 140, 130);
  text( "> Preview:  ", 40, 150);
  text( ATEMInputName(iATEM1Preview), 140, 150);
  
  if(iATEM1Program != lviATEM1Program) {
    lviATEM1Program = iATEM1Program;

    if((iATEM1Program < 2) || (iATEM1Program > 7)) {
      setKeyer(2, 0); // Lyrics / Graphics
    }
  }

  if(lvATEM != nvATEM) {
    lvATEM = nvATEM;
    if((iATEM1Program >= 2) && (iATEM1Program <= 7)) {
      setKeyer(2, nvATEM); // Lyrics / Graphics
    }
  }

}


// Optimize This!
void oscEvent(OscMessage theOscMessage) {
  String addrPattern = theOscMessage.addrPattern(); //<>//
  
  // Black
  if(addrPattern.startsWith("/atem/") == true) {
    if(addrPattern.startsWith("/program/", 5) == true) {
      if(theOscMessage.get(0).floatValue() == 1.0) {
        iATEM1Program = Integer.parseInt(addrPattern.substring(14));
      }
    } else
    if(addrPattern.startsWith("/preview/", 5) == true) {
      if(theOscMessage.get(0).floatValue() == 1.0) {
        iATEM1Preview = Integer.parseInt(addrPattern.substring(14));
      }
    } else
    if(addrPattern.startsWith("/aux/", 5) == true) {
      if(addrPattern.endsWith("/1") == true) {
        iAUX1 = theOscMessage.get(0).intValue();
      } else
      if(addrPattern.endsWith("/2") == true) {
        iAUX2 = theOscMessage.get(0).intValue();
      } else
      if(addrPattern.endsWith("/3") == true) {
        iAUX3 = theOscMessage.get(0).intValue();
      } else
      if(addrPattern.endsWith("/4") == true) {
        iAUX4 = theOscMessage.get(0).intValue();
      } else
      if(addrPattern.endsWith("/5") == true) {
        iAUX5 = theOscMessage.get(0).intValue();
      } else
      if(addrPattern.endsWith("/6") == true) {
        iAUX6 = theOscMessage.get(0).intValue();
      }
    } else
    if(addrPattern.startsWith("/usk/", 5) == true) {
      if(addrPattern.endsWith("/1") == true) {
        if(theOscMessage.get(0).intValue() == 1) {
          bATEM1USK1Status = true;
        } else {
          bATEM1USK1Status = false;
        }
      } else 
      if(addrPattern.endsWith("/2") == true) {
        if(theOscMessage.get(0).intValue() == 1) {
          bATEM1USK2Status = true;
        } else {
          bATEM1USK2Status = false;
        }
      }
    } else
    if(addrPattern.startsWith("/dskcut/", 5) == true) {
      if(addrPattern.endsWith("/1") == true) {
        if(theOscMessage.get(0).intValue() == 1) {
          bATEM1DSK1Status = true;
        } else {
          bATEM1DSK1Status = false;
        }
      } else 
      if(addrPattern.endsWith("/2") == true) {
        if(theOscMessage.get(0).intValue() == 1) {
          bATEM1DSK2Status = true;
        } else {
          bATEM1DSK2Status = false;
        }
      }
    }
  } else
  if(addrPattern.startsWith("/atem2/") == true) {
    if(addrPattern.startsWith("/program/", 6) == true) {
      if(theOscMessage.get(0).floatValue() == 1.0) {
        iATEM2Program = Integer.parseInt(addrPattern.substring(15));
      }
    } else
    if(addrPattern.startsWith("/preview/", 6) == true) {
      if(theOscMessage.get(0).floatValue() == 1.0) {
        iATEM2Preview = Integer.parseInt(addrPattern.substring(15));
      }
    } else
    if(addrPattern.startsWith("/usk/", 5) == true) {
      if(addrPattern.endsWith("/1") == true) {
        if(theOscMessage.get(0).intValue() == 1) {
          bATEM2USK1Status = true;
        } else {
          bATEM2USK1Status = false;
        }
      } else 
      if(addrPattern.endsWith("/2") == true) {
        if(theOscMessage.get(0).intValue() == 1) {
          bATEM2USK2Status = true;
        } else {
          bATEM2USK2Status = false;
        }
      }
    }
  } else {
    println(addrPattern);
  }

}